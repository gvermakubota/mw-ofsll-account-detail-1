# README #


### What is this repository for? ###

* This service retrieves account detail information from OFSLL and applies any transformations needed
* It is originally designed to be used by Paymentus
* Version: 1.0.0


### How do I get set up? ###

* application.properties files are available to customize settings for each environment
* Ensure that you have a SSL trust store available with the proper SSL certificates for testing locally



