package com.kubota.kcc.middleware.account.detail.controller;

import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.rest.exception.NotFoundException;

// Custom advice class to handle NotFoundException

@ControllerAdvice
public class AccountNotFoundAdvice {
	@Autowired
	Logger applogger;
	
	@ResponseBody
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	AccountDetailResponse accountNotFoundHandler(NotFoundException exception) {
		ObjectMapper objMapper = new ObjectMapper();
		String jsonStr = "";
		
		try {
			jsonStr = objMapper.writeValueAsString(exception.getResponse());
		} catch(Exception ex) {
			applogger.error("Error converting object to JSON: ", ex);
		}
		
		applogger.trace("Account Not Found: [{}]", (jsonStr != null ? jsonStr : exception.getResponse()));		
		return exception.getResponse();
	}
}
