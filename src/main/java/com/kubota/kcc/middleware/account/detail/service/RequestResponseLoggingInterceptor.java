package com.kubota.kcc.middleware.account.detail.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {
	Logger applogger = LogManager.getLogger("applogger");
	
	@Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
    	applogger.info("===========================request begin================================================");
    	applogger.debug("URI         : {}", request.getURI());
    	applogger.debug("Method      : {}", request.getMethod());
    	applogger.debug("Headers     : {}", request.getHeaders() );
    	applogger.debug("Request body: {}", new String(body, "UTF-8"));
    	applogger.info("==========================request end================================================");
    }

    private void traceResponse(ClientHttpResponse response) throws IOException {
        StringBuilder inputStringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
        String line = bufferedReader.readLine();
        while (line != null) {
            inputStringBuilder.append(line);
            inputStringBuilder.append('\n');
            line = bufferedReader.readLine();
        }
        applogger.info("============================response begin==========================================");
        applogger.debug("Status code  : {}", response.getStatusCode());
        applogger.debug("Status text  : {}", response.getStatusText());
        applogger.debug("Headers      : {}", response.getHeaders());
        applogger.debug("Response body: {}", inputStringBuilder.toString());
        applogger.info("=======================response end=================================================");
    }

}
