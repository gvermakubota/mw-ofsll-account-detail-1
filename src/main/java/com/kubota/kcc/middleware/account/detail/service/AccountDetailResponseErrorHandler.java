package com.kubota.kcc.middleware.account.detail.service;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.rest.exception.InternalServerException;
import com.kubota.kcc.middleware.rest.exception.NotFoundException;

// Handles errors that return from OFSLL web service call
@Component
public class AccountDetailResponseErrorHandler implements ResponseErrorHandler {
	
	Logger applogger = LogManager.getLogger("applogger");
	
	private final ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return (
				response.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR || 
				response.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		if (response.getStatusCode()
		            .series() == HttpStatus.Series.SERVER_ERROR) {
			AccountDetailResponse accountDetailResponse = mapper.readValue(response.getBody(), AccountDetailResponse.class);
		    applogger.error("OFSLL Return Status Code: [{}] OFSLL Return Status Text: [{}]", response.getStatusCode(), response.getStatusText());
		    throw new InternalServerException(accountDetailResponse.getAccountDetailResponse().getResult().getStatusDetails(), null);
		} else if(response.getStatusCode() == HttpStatus.BAD_REQUEST) {
			AccountDetailResponse accountDetailResponse = mapper.readValue(response.getBody(), AccountDetailResponse.class);
			
			applogger.error("OFSLL Return Status Code: [{}] OFSLL Return Status Text: [{}]", response.getStatusCode(), response.getStatusText());
			throw new NotFoundException(accountDetailResponse);			
		}
	}

}
