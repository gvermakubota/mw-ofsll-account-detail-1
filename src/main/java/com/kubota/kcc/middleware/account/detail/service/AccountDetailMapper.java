package com.kubota.kcc.middleware.account.detail.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountBalance;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountConditions;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailRequest;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailSummary;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Custom;
import com.kubota.kcc.middleware.account.detail.model.ofsll.CustomerDetail;
import com.kubota.kcc.middleware.account.detail.model.ofsll.DateDatum;
import com.kubota.kcc.middleware.account.detail.model.ofsll.NumberDatum;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Result;
import com.kubota.kcc.middleware.account.detail.util.Constants;

@Component
public class AccountDetailMapper {
	@Value("#{'${ofsll.legal.status.codes}'.split(',')}")
	private List<String> ofsllLegalStatusCodes;
	
	private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
	
	com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse mapPaymentusAccountDetailResponse(AccountDetailResponse inResponse) {
		com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse outResponse = new com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse();			
		outResponse.setAccountDetailResponse(mapAccountDetailResponse1(inResponse.getAccountDetailResponse().getAccountDetailRequest(),
																	   (inResponse.getAccountDetailResponse().getAccountDetailSummary().isEmpty() ? null : inResponse.getAccountDetailResponse().getAccountDetailSummary().get(0) ), 
																	   inResponse.getAccountDetailResponse().getResult()));		
		
		return outResponse;		
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse__1 mapAccountDetailResponse1(AccountDetailRequest inAccountDetailRequest, AccountDetailSummary inAccountDetailSummary, Result inResult) {
		com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse__1 outResponse1 = new com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse__1();
		
		outResponse1.setAccountDetailRequest(mapRequest(inAccountDetailRequest));
		if( inAccountDetailSummary != null)
			outResponse1.setAccountDetailSummary(mapAccountDetailSummary(inAccountDetailSummary));
		outResponse1.setResult(mapResult(inResult));
		
		return outResponse1;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailRequest mapRequest(AccountDetailRequest inRequest) {
		com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailRequest outRequest = new com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailRequest();
		outRequest.setAccountNumber(inRequest.getAccountNumber());
		
		return outRequest;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.Result mapResult(Result inResult) {
		com.kubota.kcc.middleware.account.detail.model.rest.Result outResult = new com.kubota.kcc.middleware.account.detail.model.rest.Result();
		outResult.setStatus(inResult.getStatus());
		outResult.setStatusDetails(inResult.getStatusDetails());
		
		return outResult;
	}
	
	List<com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailSummary> mapAccountDetailSummary(AccountDetailSummary inAccountDetailSummary) {
		List<com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailSummary> outAccountDetailSummaryList = new ArrayList<>();
		com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailSummary outAccountDetailSummary = new com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailSummary();

		outAccountDetailSummary.setAccountStatus(inAccountDetailSummary.getAccountStatus());
		outAccountDetailSummary.setCurrentPayOffAmount(inAccountDetailSummary.getCurrentPayOffAmount());
		outAccountDetailSummary.setCurrentMaturityDate(inAccountDetailSummary.getCurrentMaturityDate());
		outAccountDetailSummary.setDueDelinquentAmount(inAccountDetailSummary.getDueDelinquentAmount());
		outAccountDetailSummary.setDueTotalAmount(inAccountDetailSummary.getDueTotalAmount());
		outAccountDetailSummary.setFuturePayOffAmount(inAccountDetailSummary.getFuturePayOffAmount());
		outAccountDetailSummary.setFuturePayOffDate(inAccountDetailSummary.getFuturePayOffDate());
		outAccountDetailSummary.setLastPaymentAmount(inAccountDetailSummary.getLastPaymentAmount());
		outAccountDetailSummary.setLastPaymentDate(inAccountDetailSummary.getLastPaymentDate());
		outAccountDetailSummary.setNextDueDate(inAccountDetailSummary.getNextDueDate());
		outAccountDetailSummary.setNextPaymentDueAmount(inAccountDetailSummary.getNextPaymentDueAmount());
		
		// Set CustomerDetail List				
		outAccountDetailSummary.setCustomerDetails(mapCustomerDetail(inAccountDetailSummary.getCustomerDetails()));
		
		// Set Account Balances
		outAccountDetailSummary.setAccountBalances(mapAccountBalances(inAccountDetailSummary.getAccountBalances().getAccountBalance()));
				
		// Set Custom
		outAccountDetailSummary.setCustom(mapCustom(inAccountDetailSummary.getAccountConditions(), inAccountDetailSummary.getCustom()));
		
		outAccountDetailSummaryList.add(outAccountDetailSummary);
		return outAccountDetailSummaryList;
	}
	
	List<com.kubota.kcc.middleware.account.detail.model.rest.CustomerDetail> mapCustomerDetail(List<CustomerDetail> inCustomerDetailList) {
		List<com.kubota.kcc.middleware.account.detail.model.rest.CustomerDetail> customerDetailList = new ArrayList<>();
		inCustomerDetailList.forEach(inCustomerDetail -> {
			com.kubota.kcc.middleware.account.detail.model.rest.CustomerDetail customerDetail = new com.kubota.kcc.middleware.account.detail.model.rest.CustomerDetail();
			customerDetail.setRelationType(inCustomerDetail.getRelationType());
			customerDetail.setSsn(inCustomerDetail.getSsn());
			customerDetailList.add(customerDetail);
		});
		
		return customerDetailList;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.AccountBalances mapAccountBalances(List<AccountBalance> inAccountBalanceList) {
		com.kubota.kcc.middleware.account.detail.model.rest.AccountBalances outAccountBalances = new com.kubota.kcc.middleware.account.detail.model.rest.AccountBalances();
		List<com.kubota.kcc.middleware.account.detail.model.rest.AccountBalance> outAccountBalanceList = new ArrayList<>();
		
		inAccountBalanceList.forEach(inAccountBalance -> {
			if( inAccountBalance.getAvailableTransactionType().equals(Constants.ACCOUNT_BALANCE_TRANS_TYPE_INTEREST) ) {
				com.kubota.kcc.middleware.account.detail.model.rest.AccountBalance balance = new com.kubota.kcc.middleware.account.detail.model.rest.AccountBalance();
				balance.setAvailableTransactionType(inAccountBalance.getAvailableTransactionType());
				balance.setBalancePaidInCurrentYear(inAccountBalance.getBalancePaidInCurrentYear());
				balance.setBalancePaidInPreviousYear(inAccountBalance.getBalancePaidInPreviousYear());
				outAccountBalanceList.add(balance);
			}
		});
		
		outAccountBalances.setAccountBalance(outAccountBalanceList);
		
		return outAccountBalances;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.Custom mapCustom(AccountConditions inAccountConditionList, Custom inCustom ) {
		com.kubota.kcc.middleware.account.detail.model.rest.Custom outCustom = new com.kubota.kcc.middleware.account.detail.model.rest.Custom();
				
		outCustom.setStringData(mapCustomStringData(inCustom, inAccountConditionList));
		outCustom.setDateData(mapCustomDateData(inCustom));
		outCustom.setNumberData(mapCustomNumberData(inCustom));
		
		return outCustom;
	}
	
	List<com.kubota.kcc.middleware.account.detail.model.rest.StringDatum> mapCustomStringData(Custom inCustom, AccountConditions inAccountConditionList) {
		List<com.kubota.kcc.middleware.account.detail.model.rest.StringDatum> outStringDataList = new ArrayList<>();
		
		outStringDataList.add(mapLegalStatus(inAccountConditionList));

		return outStringDataList;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.StringDatum mapLegalStatus(AccountConditions inAccountConditionList) {
		com.kubota.kcc.middleware.account.detail.model.rest.StringDatum outStringData = new com.kubota.kcc.middleware.account.detail.model.rest.StringDatum();
		
		outStringData.setKeyName(Constants.LEGAL_STATUS_STR_DATA_KEY_NAME);
		outStringData.setKeyValue(Constants.N_STR_DATA_KEY_VALUE);

		if( inAccountConditionList != null )
			inAccountConditionList.getAccountCondition().stream()
														.filter(inCondition -> ofsllLegalStatusCodes.contains(inCondition.getConditionCode()))
														.findFirst()
														.ifPresent(inCondition -> outStringData.setKeyValue(Constants.Y_STR_DATA_KEY_VALUE) );
			

		return outStringData;
	}
	
	List<com.kubota.kcc.middleware.account.detail.model.rest.DateDatum> mapCustomDateData(Custom inCustom) {
		List<com.kubota.kcc.middleware.account.detail.model.rest.DateDatum> outDateDataList = new ArrayList<>();
		
		if( inCustom != null ) {
			com.kubota.kcc.middleware.account.detail.model.rest.DateDatum futurePaymentDate = mapFuturePaymentDate(inCustom.getDateData());
			com.kubota.kcc.middleware.account.detail.model.rest.DateDatum paidOffDate = mapPaidOffDate(inCustom.getDateData());
			com.kubota.kcc.middleware.account.detail.model.rest.DateDatum oldestDueDate = mapOldestDueDate(inCustom.getDateData());
			
			if( futurePaymentDate.getKeyName() != null )
				outDateDataList.add(futurePaymentDate);
			
			if( paidOffDate.getKeyName() != null )
				outDateDataList.add(paidOffDate);
			
			if( oldestDueDate.getKeyName() != null )
				outDateDataList.add(oldestDueDate);
		}

		return outDateDataList;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.DateDatum mapFuturePaymentDate(List<DateDatum> inDateDataList) {
		com.kubota.kcc.middleware.account.detail.model.rest.DateDatum outFuturePaymentDate = new com.kubota.kcc.middleware.account.detail.model.rest.DateDatum();
				
		inDateDataList.stream().filter(inDateData -> Constants.FUTURE_PMT_DT_DATE_DATA_KEY_NAME.equals(inDateData.getKeyName()))
							   .findFirst()
							   .ifPresent(inDateData -> {
								   outFuturePaymentDate.setKeyName(Constants.FUTURE_PMT_DT_DATE_DATA_KEY_NAME);
								   outFuturePaymentDate.setKeyValue(outputCustomDate(inDateData.getKeyValue()));
							   });
		
		return outFuturePaymentDate;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.DateDatum mapPaidOffDate(List<DateDatum> inDateDataList) {
		com.kubota.kcc.middleware.account.detail.model.rest.DateDatum outPaidOffDate = new com.kubota.kcc.middleware.account.detail.model.rest.DateDatum();
				
		inDateDataList.stream().filter(inDateData -> Constants.PAID_OFF_DT_DATE_DATA_KEY_NAME.equals(inDateData.getKeyName()))
							   .findFirst()
							   .ifPresent(inDateData -> {
								   outPaidOffDate.setKeyName(Constants.PAID_OFF_DT_DATE_DATA_KEY_NAME);
								   outPaidOffDate.setKeyValue(outputCustomDate(inDateData.getKeyValue()));									   
							   });
		
		return outPaidOffDate;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.DateDatum mapOldestDueDate(List<DateDatum> inDateDataList) {
		com.kubota.kcc.middleware.account.detail.model.rest.DateDatum outOldestDueDate = new com.kubota.kcc.middleware.account.detail.model.rest.DateDatum();
				
		inDateDataList.stream().filter(inDateData -> Constants.OLDEST_DUE_DT_DATE_DATA_KEY_NAME.equals(inDateData.getKeyName()))
							   .findFirst()
							   .ifPresent(inDateData -> {
								   outOldestDueDate.setKeyName(Constants.OLDEST_DUE_DT_DATE_DATA_KEY_NAME);
								   outOldestDueDate.setKeyValue(outputCustomDate(inDateData.getKeyValue()));									   
							   });
		
		return outOldestDueDate;
	}
	
	List<com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum> mapCustomNumberData(Custom inCustom) {
		List<com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum> outNumberDataList = new ArrayList<>();
		
		if( inCustom != null )
			outNumberDataList.add(mapFuturePaymentAmount(inCustom.getNumberData()));

		return outNumberDataList;
	}
	
	com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum mapFuturePaymentAmount(List<NumberDatum> inNumberDataList) {
		com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum outFuturePaymentAmount = new com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum();
				
		inNumberDataList.stream().filter(inNumberData -> Constants.FUTURE_PMT_AMT_NUMBER_KEY_NAME.equals(inNumberData.getKeyName()))
							   .findFirst()
							   .ifPresent(inNumberData -> {
								   outFuturePaymentAmount.setKeyName(Constants.FUTURE_PMT_AMT_NUMBER_KEY_NAME);
								   outFuturePaymentAmount.setKeyValue(inNumberData.getKeyValue());									   
							   });
		
		return outFuturePaymentAmount;
	}
	
	/* 
	 * outputCustomDate method used to convert OFSLL custom dates from "yyyy-MM-dd HH:mm:ss.S" format to match 
	 * other OFSLL date formats ("yyyy-MM-ddTHH:mm:ss-05:00").
	 * This is a workaround, as the OFSLL base product has a bug that does not output in consistent date/time format.
	 * When OFSLL product is fixed, we will not need to use this method to transform date/time.
	 * JIRA: PAYS-58
	 * */
	String outputCustomDate(String inDateStr) {
		String outDateStr = "";
		
		if( inDateStr != null && inDateStr.length() > 0 ) {
			LocalDateTime localDateTime = LocalDateTime.parse(inDateStr, dateTimeFormatter);
			ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("-5"));
			outDateStr = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(zonedDateTime);
		} 
		
		return outDateStr ;
	}
}
