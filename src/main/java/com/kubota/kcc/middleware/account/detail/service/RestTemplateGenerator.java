package com.kubota.kcc.middleware.account.detail.service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateGenerator {
	@Value("${http.client.ssl.hostname-verify}")
	private Boolean hostnameVerify;
	
	@Value("${http.client.ssl.trust-store}")
    private FileSystemResource trustStore;
	
    @Value("${mw.java.truststore.password}")
    private String trustStorePassword;
    
    @Value("${oracle.endpoint.username}")
	private String ofsllRestUser;
	
	@Value("${oracle.endpoint.password}")
	private String ofsllRestPassword;
	
	@Value("${ofsll.endpoint.account.detail.connect.timeout}")
	private Integer ofsllConnectTimeout;
	
	@Value("${ofsll.endpoint.account.detail.read.timeout}")
	private Integer ofsllReadTimeout;
    
    @Bean
    public RestTemplate restTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
    	SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(trustStore.getFile(), trustStorePassword.toCharArray()).build();
    	    	
    	SSLConnectionSocketFactory socketFactory = null;
    	
    	if( Boolean.TRUE.equals(hostnameVerify) )
    		socketFactory = new SSLConnectionSocketFactory(sslContext);
    	else
    		socketFactory = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
    	
    	CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    	 
        credentialsProvider.setCredentials(AuthScope.ANY, 
                        new UsernamePasswordCredentials(ofsllRestUser, ofsllRestPassword));
        
    	HttpClient httpClient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).setSSLSocketFactory(socketFactory).build();
    	
    	HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
    	factory.setConnectTimeout(ofsllConnectTimeout);
    	factory.setReadTimeout(ofsllReadTimeout);
    	
    	// Add wrapper BufferingClientHttpRequestFactory to be able to log request/response and process each
    	BufferingClientHttpRequestFactory bufferingRequestFactory = new BufferingClientHttpRequestFactory(factory);
    	
    	List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
    	interceptors.add(new RequestResponseLoggingInterceptor());
    	
    	RestTemplate restTemplate = new RestTemplate(bufferingRequestFactory);
    	restTemplate.setErrorHandler(new AccountDetailResponseErrorHandler());
    	restTemplate.setInterceptors(interceptors);
    	
    	return restTemplate;
    }
}
