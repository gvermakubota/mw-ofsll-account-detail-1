package com.kubota.kcc.middleware.account.detail.controller;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse;
import com.kubota.kcc.middleware.account.detail.service.AccountDetailService;

@RestController
public class AccountDetailController {
	@Autowired
	Logger applogger;
	
	@Autowired
	AccountDetailService accountDetailService;
	
	@GetMapping(path = "/account/detail/{id}", produces = "application/json")
	@ResponseBody
	public AccountDetailResponse getAccountDetail(@PathVariable("id") String accountId) {		
		return accountDetailService.getAccountDetail(accountId);
	}

}
