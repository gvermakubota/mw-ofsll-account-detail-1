package com.kubota.kcc.middleware.account.detail.controller;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailRequest;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse__1;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Result;
import com.kubota.kcc.middleware.rest.exception.InternalServerException;

// Custom advice class to handle service errors (NPE, timeout, etc.)

@ControllerAdvice
public class InternalServerErrorAdvice {
	@Autowired
	Logger applogger;
	
	@ResponseBody
	@ExceptionHandler(InternalServerException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	AccountDetailResponse internalServerErrorHandler(InternalServerException exception) {
		applogger.error("Internal Server Error: ", exception);
		applogger.fatal("Internal Server Error: ", exception);
		AccountDetailResponse response = new AccountDetailResponse();
		AccountDetailResponse__1 accountDetail = new AccountDetailResponse__1();
		AccountDetailRequest request = new AccountDetailRequest();
		Result result = new Result();
		
		request.setAccountNumber(exception.getAccountId());		
		accountDetail.setAccountDetailRequest(request);
		
		result.setStatus("ERROR");
		result.setStatusDetails("Internal Server Error. Contact KCC App Support Team.");
		accountDetail.setResult(result);
		
		response.setAccountDetailResponse(accountDetail);
		
		return response;
	}
}
