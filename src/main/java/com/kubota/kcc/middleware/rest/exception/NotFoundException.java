package com.kubota.kcc.middleware.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
	private static final long serialVersionUID = -5112566524754855037L;

	private final AccountDetailResponse response;
	
	public NotFoundException(AccountDetailResponse response) {
		this.response = response;
	}

	public AccountDetailResponse getResponse() {
		return response;
	}
}
