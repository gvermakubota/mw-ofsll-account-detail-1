Feature: OFSLL Account Detail Retrieval

	Scenario: Valid OFSLL Account Number Provided
	 
	Given an account exists with an account ID of "61884755" 
	When a system retrieves the account by account ID
	Then the status code is 200
	
	Scenario: Invalid OFSLL Account Number Provided
	
	Given an account does not exist with an account ID of "11122233"
	When a system retrieves the account by account ID
	Then the status code is 404
	
	Scenario: Account with Specific Condition Codes Needs To Indicate Legal Status
	
	Given an account exists with an account ID of "61872701"
	And contains legal condition codes
	When a system retrieves the account by account ID
	Then the response should contain a custom element indicating legal status is "Y"
	
	Scenario: Account without Specific Condition Codes Needs To Indicate No Legal Status
	
	Given an account exists with an account ID of "61884755"
	And does not contains legal condition codes
	When a system retrieves the account by account ID
	Then the response should contain a custom element indicating legal status is "N"
	
	Scenario: Account in CHGOFF or VOID Status Should not Be Returned
	
	Given a valid account in CHGOFF or VOID status of "12345678"
	When a system retrieves the account by account ID
	Then the status code is 404
	
	Scenario: Account has been in PAIDOFF Status for <= 270 Days Should be Returned
	
	Given an account exists with an account ID of "12345678"
	And the account is in PAIDOFF status
	And the account was paid off less than 270 days from current date
	When a system retrieves the account by account ID
	Then return the account details and 200 response
	
	Scenario: Account has been in PAIDOFF Status for > 270 Days Should be Returned
	
	Given an account exists with an account ID of "12345678"
	And the account is in PAIDOFF status
	And the account was paid off more than 270 days from current date
	When a system retrieves the account by account ID
	Then the status code is 404