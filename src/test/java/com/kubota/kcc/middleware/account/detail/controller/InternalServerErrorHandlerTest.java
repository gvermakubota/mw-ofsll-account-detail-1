package com.kubota.kcc.middleware.account.detail.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.rest.exception.InternalServerException;

@SpringBootTest(classes = Logger.class)
public class InternalServerErrorHandlerTest {
	@Spy
	Logger applogger;
	
	@InjectMocks
	private InternalServerErrorAdvice advice;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void internalServerErrorHandlerReturnsAccountDetailResponseResponse() {
		String accountId = "12345678";

		InternalServerException exception = new InternalServerException("Exception Occurred", accountId); 
		AccountDetailResponse response = advice.internalServerErrorHandler(exception);
		
		assertNotNull(response);
		assertNotNull(response.getAccountDetailResponse());
		assertNotNull(response.getAccountDetailResponse().getResult());
		assertNotNull(response.getAccountDetailResponse().getAccountDetailRequest());
		assertEquals(accountId, response.getAccountDetailResponse().getAccountDetailRequest().getAccountNumber());
		assertEquals("ERROR", response.getAccountDetailResponse().getResult().getStatus());
		assertEquals("Internal Server Error. Contact KCC App Support Team.", response.getAccountDetailResponse().getResult().getStatusDetails());
		
	}
}
