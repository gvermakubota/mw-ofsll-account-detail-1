package com.kubota.kcc.middleware.account.detail.service;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.ssl.SSLContextBuilder;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ActiveProfiles("dev")
@ContextConfiguration(classes = RestTemplateGenerator.class)
public class RestTemplateGeneratorTest {
	@Autowired
	RestTemplateGenerator generator;
	    
	@Value("${http.client.ssl.trust-store}")
    private FileSystemResource trustStore;
	
    @Value("${mw.java.truststore.password}")
    private String trustStorePassword;
	
    /*
	@Test
	public void hostnameVerifyTrueReturnsProperSSLConnectionSocketFactory() throws Exception {
		SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(trustStore.getFile(), trustStorePassword.toCharArray()).build();
		
		SSLConnectionSocketFactory factory = generator.generateSSLConnectionSocketFactory(sslContext);
		assertNotNull(factory);
	}
	*/
}
