package com.kubota.kcc.middleware.account.detail.controller;

import static org.junit.jupiter.api.Assertions.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
class AccountDetailControllerTest {
	@Autowired
    private MockMvc mockMvc;
	
	private static final String REST_ACCOUNT_DETAIL_URI = "/account/detail/";
	
	@Test
	public void testAccountNotFoundHTTP404() throws Exception {
		String accountId = "123";
		testAccountDetailNotFound(accountId);
	}
	
	@Test
	public void testAccountFoundHTTP200() throws Exception {
		String accountId = "61884755";
		testAccountDetailFound(accountId);
	}
	
	private void testAccountDetailNotFound(String accountId) throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(REST_ACCOUNT_DETAIL_URI + accountId)
								  .accept(MediaType.APPLICATION_JSON))
								  .andDo(print())
								  .andExpect(status().isNotFound())  // Check that 404 status is returned
								  .andExpect(jsonPath("$.AccountDetailResponse.Result.Status", "ERROR").exists())  // Check that Result Status = ERROR
								  .andReturn();
		String resultStr = result.getResponse().getContentAsString();
		assertNotNull(resultStr);			// Response should always contain data
		assertNotEquals(resultStr, "");
	}
	
	private void testAccountDetailFound(String accountId) throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(REST_ACCOUNT_DETAIL_URI + accountId)
								  .accept(MediaType.APPLICATION_JSON))
								  .andDo(print())
								  .andExpect(status().isOk())        // Check that 200 status is returned
								  .andExpect(jsonPath("$.AccountDetailResponse.Result.Status", "SUCCESS").exists())  // Check that Result Status = SUCCESS
								  .andReturn();
		String resultStr = result.getResponse().getContentAsString();
		assertNotNull(resultStr);           // Response should always contain data
		assertNotEquals(resultStr, "");
	}
	
	

}
