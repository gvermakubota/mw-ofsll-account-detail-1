package com.kubota.kcc.middleware.account.detail.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountCondition;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountConditions;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Custom;
import com.kubota.kcc.middleware.account.detail.model.ofsll.DateDatum;
import com.kubota.kcc.middleware.account.detail.model.ofsll.NumberDatum;
import com.kubota.kcc.middleware.account.detail.model.rest.StringDatum;
import com.kubota.kcc.middleware.account.detail.util.Constants;

@SpringBootTest
public class AccountDetailMapperTest {
	@Autowired
	AccountDetailMapper mapper;
	
	@Value("#{'${ofsll.legal.status.codes}'.split(',')}")
	private List<String> ofsllLegalStatusCodes;
	
	private List<String> unflaggedConditionCodes = List.of("DELQ","ABC","123");
	
	
	@Test
	public void flaggedConditionCodesCreateLegalStatusY() {
		AccountConditions accountConditions = new AccountConditions();
		List<AccountCondition> accountConditionList = new ArrayList<>();
				
		ofsllLegalStatusCodes.forEach(legalStatus -> {
			AccountCondition accountCondition = new AccountCondition();
			accountCondition.setConditionCode(legalStatus);
			accountConditionList.add(accountCondition);
		});
		
		accountConditions.setAccountCondition(accountConditionList);
		
		StringDatum stringData = mapper.mapLegalStatus(accountConditions);
		assertEquals(Constants.LEGAL_STATUS_STR_DATA_KEY_NAME, stringData.getKeyName());
		assertEquals(Constants.Y_STR_DATA_KEY_VALUE, stringData.getKeyValue());
	}
	
	@Test
	public void unflaggedConditionCodesCreateLegalStatusN() {
		AccountConditions accountConditions = new AccountConditions();
		List<AccountCondition> accountConditionList = new ArrayList<>();
				
		unflaggedConditionCodes.forEach(legalStatus -> {
			AccountCondition accountCondition = new AccountCondition();
			accountCondition.setConditionCode(legalStatus);
			accountConditionList.add(accountCondition);
		});
		
		accountConditions.setAccountCondition(accountConditionList);
		
		StringDatum stringData = mapper.mapLegalStatus(accountConditions);
		assertEquals(Constants.LEGAL_STATUS_STR_DATA_KEY_NAME, stringData.getKeyName());
		assertEquals(Constants.N_STR_DATA_KEY_VALUE, stringData.getKeyValue());
	}
	
	@Test
	public void emptyConditionCodesCreateLegalStatusN() {
		AccountConditions inAccountConditionList = null;
		
		com.kubota.kcc.middleware.account.detail.model.rest.StringDatum outStringData = mapper.mapLegalStatus(inAccountConditionList);
		assertEquals(Constants.LEGAL_STATUS_STR_DATA_KEY_NAME, outStringData.getKeyName());
		assertEquals(Constants.N_STR_DATA_KEY_VALUE, outStringData.getKeyValue());
	}
	
	@Test
	public void nullDateDataDoesNotThrowException() {
		Custom inCustom = null;
		List<com.kubota.kcc.middleware.account.detail.model.rest.DateDatum> outDateDataList = mapper.mapCustomDateData(inCustom);
		
		assertNotNull(outDateDataList);
	}
	
	@Test
	public void emptyPaidOffDateDoesNotThrowException() {
		Custom inCustom = new Custom();
		List<DateDatum> emptyDateList = new ArrayList<>();
		inCustom.setDateData(emptyDateList);
		List<com.kubota.kcc.middleware.account.detail.model.rest.DateDatum> outDateDataList = mapper.mapCustomDateData(inCustom);
		
		assertNotNull(outDateDataList);
	}
	
	@Test
	public void emptyNumberDataDoesNotThrowException() {
		Custom inCustom = null;
		List<com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum> outNumberDataList = mapper.mapCustomNumberData(inCustom);
		
		assertNotNull(outNumberDataList);
	}
	
	@Test
	public void mapFuturePaymentDateReturnsCorrectObject() {
		String futureDateStr = "2019-03-20 00:00:00.0";
		String outFutureDateStr = "2019-03-20T00:00:00-05:00";
		
		List<DateDatum> inDateDataList = new ArrayList<>();		
		DateDatum inDateDatum = new DateDatum();
		
		inDateDatum.setKeyName(Constants.FUTURE_PMT_DT_DATE_DATA_KEY_NAME);
		inDateDatum.setKeyValue(futureDateStr);
		inDateDataList.add(inDateDatum);
		
		com.kubota.kcc.middleware.account.detail.model.rest.DateDatum outDateDatum = mapper.mapFuturePaymentDate(inDateDataList);
		assertEquals(Constants.FUTURE_PMT_DT_DATE_DATA_KEY_NAME, outDateDatum.getKeyName());
		assertEquals(outFutureDateStr, outDateDatum.getKeyValue());
	}
	
	@Test
	public void mapPaidOffDateReturnsCorrectObject() {
		String paidOffDateStr = "2019-03-20 00:00:00.0";
		String outPaidOffDateStr = "2019-03-20T00:00:00-05:00";
		
		List<DateDatum> inDateDataList = new ArrayList<>();		
		DateDatum inDateDatum = new DateDatum();
		
		inDateDatum.setKeyName(Constants.PAID_OFF_DT_DATE_DATA_KEY_NAME);
		inDateDatum.setKeyValue(paidOffDateStr);
		inDateDataList.add(inDateDatum);
		
		com.kubota.kcc.middleware.account.detail.model.rest.DateDatum outDateDatum = mapper.mapPaidOffDate(inDateDataList);
		assertEquals(Constants.PAID_OFF_DT_DATE_DATA_KEY_NAME, outDateDatum.getKeyName());
		assertEquals(outPaidOffDateStr, outDateDatum.getKeyValue());
	}
	
	@Test
	public void mapOldestDueDateReturnsCorrectObject() {
		String oldestDueDateStr = "2019-03-20 00:00:00.0";
		String outDueDateStr = "2019-03-20T00:00:00-05:00";
		
		List<DateDatum> inDateDataList = new ArrayList<>();		
		DateDatum inDateDatum = new DateDatum();
		
		inDateDatum.setKeyName(Constants.OLDEST_DUE_DT_DATE_DATA_KEY_NAME);
		inDateDatum.setKeyValue(oldestDueDateStr);
		inDateDataList.add(inDateDatum);
		
		com.kubota.kcc.middleware.account.detail.model.rest.DateDatum outDateDatum = mapper.mapOldestDueDate(inDateDataList);
		assertEquals(Constants.OLDEST_DUE_DT_DATE_DATA_KEY_NAME, outDateDatum.getKeyName());
		assertEquals(outDueDateStr, outDateDatum.getKeyValue());
	}
	
	@Test
	public void mapFuturePaymentAmountReturnsCorrectObject() {
		Double futurePaymentAmount = 1234.00;
		
		List<NumberDatum> inNumberDataList = new ArrayList<>();		
		NumberDatum inNumberDatum = new NumberDatum();
		
		inNumberDatum.setKeyName(Constants.FUTURE_PMT_AMT_NUMBER_KEY_NAME);
		inNumberDatum.setKeyValue(futurePaymentAmount);
		inNumberDataList.add(inNumberDatum);
		
		com.kubota.kcc.middleware.account.detail.model.rest.NumberDatum outDateDatum = mapper.mapFuturePaymentAmount(inNumberDataList);
		assertEquals(Constants.FUTURE_PMT_AMT_NUMBER_KEY_NAME, outDateDatum.getKeyName());
		assertEquals(futurePaymentAmount, outDateDatum.getKeyValue());
	}
		
}
