package com.kubota.kcc.middleware.account.detail.service;



import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse__1;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailSummary;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Custom;
import com.kubota.kcc.middleware.account.detail.model.ofsll.DateDatum;
import com.kubota.kcc.middleware.account.detail.util.Constants;
import com.kubota.kcc.middleware.rest.exception.InternalServerException;
import com.kubota.kcc.middleware.rest.exception.NotFoundException;



@SpringBootTest
@ActiveProfiles("uat")
public class AccountDetailServiceTest {
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	AccountDetailService service;
	
	@Value("${ofsll.endpoint.account.detail.url}")
	private String ofsllAccountDetailEndpoint;
	
	@Test
	void testAccountsNotSentToPaymentus() throws Exception {
		chargeOffAccountThrowsNotFoundException();
		voidAccountThrowsNotFoundException();
		paidoffAccountGT270DaysThrowsNotFoundException();
	}
	
	@Test
	void testAccountSentToPaymentus() throws Exception {
		paidOffAccountLT270DaysReturnsData();
		activeAccountReturnsData();
	}
		
	public void chargeOffAccountThrowsNotFoundException() throws Exception {
		String chgoffAccount = "99887766";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/CHGOFF_OFSLL_Account.json").toURI();
		String invalidAccountResponseJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + chgoffAccount)).andRespond(withSuccess()
																			 	.body(invalidAccountResponseJSON)
																			 	.contentType(MediaType.APPLICATION_JSON));
		
		assertThrows(NotFoundException.class, () -> service.getAccountDetail(chgoffAccount));		
	}
	
	public void voidAccountThrowsNotFoundException() throws Exception {
		String voidAccount = "99887755";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/VOID_OFSLL_Account.json").toURI();
		String invalidAccountResponseJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + voidAccount)).andRespond(withSuccess()
																			  .body(invalidAccountResponseJSON)
																			  .contentType(MediaType.APPLICATION_JSON));
		
		assertThrows(NotFoundException.class, () -> service.getAccountDetail(voidAccount));		
	}
	
	public void paidoffAccountGT270DaysThrowsNotFoundException() throws Exception {
		String paidoffAccount = "99887744";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/PAIDOFF_OFSLL_ACCOUNT_GT_270d.json").toURI();
		String invalidAccountResponseJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + paidoffAccount)).andRespond(withSuccess()
																			  	 .body(invalidAccountResponseJSON)
																			  	 .contentType(MediaType.APPLICATION_JSON));
		
		assertThrows(NotFoundException.class, () -> service.getAccountDetail(paidoffAccount));		
	}
	
	public void paidOffAccountLT270DaysReturnsData() throws Exception {
		String paidoffAccount = "99887744";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/PAIDOFF_OFSLL_ACCOUNT_LT_270d.json").toURI();
		String paidOffAccountResponseJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + paidoffAccount)).andRespond(withSuccess()
																			  	 .body(paidOffAccountResponseJSON)
																			  	 .contentType(MediaType.APPLICATION_JSON));
		
		assertEquals("SUCCESS", service.getAccountDetail(paidoffAccount).getAccountDetailResponse().getResult().getStatus());	
	}
	
	public void activeAccountReturnsData() throws Exception {
		String validAccount = "99887733";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/VALID_OFSLL_Account.json").toURI();
		String invalidAccountResponseJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + validAccount)).andRespond(withSuccess()
																			   .body(invalidAccountResponseJSON)
																			   .contentType(MediaType.APPLICATION_JSON));
		
		assertEquals("SUCCESS", service.getAccountDetail(validAccount).getAccountDetailResponse().getResult().getStatus());
	}
	
	@Test
	public void nullCustomObjectDoesNotThrowExceptionRetrievingPaidOffDate() throws Exception {
		AccountDetailResponse response = new AccountDetailResponse();
		AccountDetailResponse__1 response1 = new AccountDetailResponse__1();
		
		List<AccountDetailSummary> accountDetailSummaryList = new ArrayList<>();		
		
		AccountDetailSummary accountDetailSummary = new AccountDetailSummary();
		accountDetailSummary.setCustom(null);
		
		accountDetailSummaryList.add(accountDetailSummary);
		
		response1.setAccountDetailSummary(accountDetailSummaryList);
		
		response.setAccountDetailResponse(response1);
		
		String paidOffDate = service.getPaidoffDate(response);
		assertNotNull(paidOffDate);
		assertEquals("", paidOffDate);
	}
	
	@Test
	public void getPaidOffDateReturnsCorrectDataWhenPaidOffDateSent() throws Exception {
		String paidOffDateStr = "2019-03-20 00:00:00.0";
		AccountDetailResponse response = new AccountDetailResponse();
		AccountDetailResponse__1 response1 = new AccountDetailResponse__1();
		
		List<AccountDetailSummary> accountDetailSummaryList = new ArrayList<>();		
		
		AccountDetailSummary accountDetailSummary = new AccountDetailSummary();
		
		Custom custom = new Custom();
		
		List<DateDatum> dateDataList = new ArrayList<>();
		
		DateDatum paidOffDateDatum = new DateDatum();		
		paidOffDateDatum.setKeyName(Constants.PAID_OFF_DT_DATE_DATA_KEY_NAME);
		paidOffDateDatum.setKeyValue(paidOffDateStr);
		
		dateDataList.add(paidOffDateDatum);
		
		custom.setDateData(dateDataList);		
		
		accountDetailSummary.setCustom(custom);
		
		accountDetailSummaryList.add(accountDetailSummary);
		
		response1.setAccountDetailSummary(accountDetailSummaryList);
		
		response.setAccountDetailResponse(response1);
		
		String paidOffDate = service.getPaidoffDate(response);
		assertNotNull(paidOffDate);
		assertEquals(paidOffDateStr, paidOffDate);
	}
	
	@Test
	public void getPaidOffDateReturnsStringValueWhenNoPaidOffDateSent() throws Exception {
		AccountDetailResponse response = new AccountDetailResponse();
		AccountDetailResponse__1 response1 = new AccountDetailResponse__1();
		
		List<AccountDetailSummary> accountDetailSummaryList = new ArrayList<>();		
		
		AccountDetailSummary accountDetailSummary = new AccountDetailSummary();
		
		Custom custom = new Custom();
		
		List<DateDatum> dateDataList = new ArrayList<>();
		
		DateDatum dateDatum = new DateDatum();
		dateDatum.setKeyName("TEST");
		dateDatum.setKeyValue("2019-03-20 00:00:00.0");
		
		dateDataList.add(dateDatum);
		
		custom.setDateData(dateDataList);		
		
		accountDetailSummary.setCustom(custom);
		
		accountDetailSummaryList.add(accountDetailSummary);
		
		response1.setAccountDetailSummary(accountDetailSummaryList);
		
		response.setAccountDetailResponse(response1);
		
		String paidOffDate = service.getPaidoffDate(response);
		assertNotNull(paidOffDate);
		assertEquals("", paidOffDate);
	}
	
	@Test
	public void dateOlderThanDaysReturnsFalseForNullOrEmptyInDate() {	
		assertEquals(false, service.dateOlderThanDays(null, 270));
		assertEquals(false, service.dateOlderThanDays("", 270));
	}
	
	@Test
	public void getAccountDetailHandlesException() throws Exception {
		String account = "99887711";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/ServerError_OFSLL_Response.json").toURI();
		String serverErrorJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + account)).andRespond(withServerError()
																					  .body(serverErrorJSON)
																					  .contentType(MediaType.APPLICATION_JSON));
		
		assertThrows(InternalServerException.class, () -> service.getAccountDetail(account));		
	}
	
	@Test
	public void serviceReturnsNotFoundWhenOFSLLReturns400Error() throws Exception {
		String testAccount = "99887733";
		
		MockRestServiceServer mockServer = MockRestServiceServer.bindTo(restTemplate).bufferContent().build();
		URI uri = AccountDetailServiceTest.class.getClassLoader().getResource("mockdata/AccountDataNotFound_OFSLL_Response.json").toURI();
		String invalidAccountResponseJSON = new String(Files.readAllBytes(Paths.get(uri)));
		
		mockServer.expect(requestTo(ofsllAccountDetailEndpoint + testAccount)).andRespond(withBadRequest()
																			   			  .body(invalidAccountResponseJSON)
																			   			  .contentType(MediaType.APPLICATION_JSON));
		
		assertThrows(NotFoundException.class, () -> service.getAccountDetail(testAccount));		
	}
}
